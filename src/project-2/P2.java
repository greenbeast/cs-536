import java.util.*;
import java.io.*;
import java_cup.runtime.*;  // defines Symbol

/** ͙
 * This program is to be used to test the Gibberish scanner.
 * This version is set up to test all tokens, but you are required to test
 * other aspects of the scanner (e.g., input that causes errors, character
 * numbers, values associated with tokens)
 */
public class P2 {
    public static void main(String[] args) throws IOException {
        // exception may be thrown by yylex

        testAllTokens(args[0]);

        // ADD CALLS TO OTHER TEST METHODS HERE
    }

    /**
     * testAllTokens
     *
     * Open and read from file allTokens.txt
     * For each token read, write the corresponding string to allTokens.out
     * If the input file contains all tokens, one per line, we can verify
     * correctness of the scanner by comparing the input and output files
     * (e.g., using a 'diff' command).
     */
    private static void testAllTokens(String input_file) throws IOException {
        // open input and output files
        FileReader inFile = null;
        try {
            inFile = new FileReader(input_file);
        } catch (FileNotFoundException ex) {
            System.err.printf("File %s not found.", input_file);
            System.exit(-1);
        }

        // create and call the scanner
        Yylex scanner = new Yylex(inFile);
        Symbol token = scanner.next_token();
        while (token.sym != sym.EOF) {
            switch (token.sym) {
            case sym.BOOL:
                System.out.println("bool");
                break;
            case sym.INT:
                System.out.println("int");
                break;
            case sym.VOID:
                System.out.println("void");
                break;
            case sym.TRUE:
                System.out.println("true");
                break;
            case sym.FALSE:
                System.out.println("false");
                break;
            case sym.STRUCT:
                System.out.println("struct");
                break;
            case sym.CIN:
                System.out.println("cin");
                break;
            case sym.COUT:
                System.out.println("cout");
                break;
            case sym.IF:
                System.out.println("if");
                break;
            case sym.ELSE:
                System.out.println("else");
                break;
            case sym.WHILE:
                System.out.println("while");
                break;
            case sym.RETURN:
                System.out.println("return");
                break;
            case sym.ID:
                System.out.println(((IdTokenVal)token.value).idVal);
                break;
            case sym.INTLITERAL:
                System.out.println(((IntLitTokenVal)token.value).intVal);
                break;
            case sym.STRINGLITERAL:
                System.out.println(((StrLitTokenVal)token.value).strVal);
                break;
            case sym.LCURLY:
                System.out.println("{");
                break;
            case sym.RCURLY:
                System.out.println("}");
                break;
            case sym.LPAREN:
                System.out.println("(");
                break;
            case sym.RPAREN:
                System.out.println(")");
                break;
            case sym.SEMICOLON:
                System.out.println(";");
                break;
            case sym.COMMA:
                System.out.println(",");
                break;
            case sym.DOT:
                System.out.println(".");
                break;
            case sym.WRITE:
                System.out.println("<<");
                break;
            case sym.READ:
                System.out.println(">>");
                break;
            case sym.PLUSPLUS:
                System.out.println("++");
                break;
            case sym.MINUSMINUS:
                System.out.println("--");
                break;
            case sym.PLUS:
                System.out.println("+");
                break;
            case sym.MINUS:
                System.out.println("-");
                break;
            case sym.TIMES:
                System.out.println("*");
                break;
            case sym.DIVIDE:
                System.out.println("/");
                break;
            case sym.NOT:
                System.out.println("!");
                break;
            case sym.AND:
                System.out.println("&&");
                break;
            case sym.OR:
                System.out.println("||");
                break;
            case sym.EQUALS:
                System.out.println("==");
                break;
            case sym.NOTEQUALS:
                System.out.println("!=");
                break;
            case sym.LESS:
                System.out.println("<");
                break;
            case sym.GREATER:
                System.out.println(">");
                break;
            case sym.LESSEQ:
                System.out.println("<=");
                break;
            case sym.GREATEREQ:
                System.out.println(">=");
                break;
            case sym.ASSIGN:
                System.out.println("=");
                break;
            default:
                System.out.println("UNKNOWN TOKEN");
            } // end switch

            token = scanner.next_token();
        } // end while
        //outFile.close();
    }
}
