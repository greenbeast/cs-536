import java.util.LinkedList;

public class TSym {
	private String type;
	private SymTable symTable;
	private StructDeclNode struct;

	public TSym(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public String toString() {
		return type;
	}

	public void setStruct(StructDeclNode struct) {
		this.struct = struct;
	}

	public StructDeclNode getStruct() {
		return struct;
	}

	public void setSymTable(SymTable symTable) {
		this.symTable = symTable;
	}

	public SymTable getSymTable() {
		return symTable;
	}
}

class fnSym extends TSym {
	private LinkedList<String> params;
	private String returnType;

	public fnSym(String returnType, LinkedList<String> params) {
		super("function");
		this.params = params;
		this.returnType = returnType;
	}

	public int getNumParams() {
		return params.size();
	}

	public String getReturnType() {
		return returnType;
	}

	public String toString() {
		String params = String.join(", ", this.params);
		if (params.length() == 0) {
			params = "void";
		}
		return returnType + " (" + params + ")";
	}
}
