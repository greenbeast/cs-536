import java.io.*;
import java.util.*;

// **********************************************************************
// The ASTnode class defines the nodes of the abstract-syntax tree that
// represents a C-- program.
//
// Internal nodes of the tree contain pointers to children, organized
// either in a list (for nodes that may have a variable number of
// children) or as a fixed set of fields.
//
// The nodes for literals and ids contain line and character number
// information; for string literals and identifiers, they also contain a
// string; for integer literals, they also contain an integer value.
//
// Here are all the different kinds of AST nodes and what kinds of children
// they have.  All of these kinds of AST nodes are subclasses of "ASTnode".
// Indentation indicates further subclassing:
//
//     Subclass            Children
//     --------            ----
//     ProgramNode         DeclListNode
//     DeclListNode        linked list of DeclNode
//     DeclNode:
//       VarDeclNode       TypeNode, IdNode, int
//       FnDeclNode        TypeNode, IdNode, FormalsListNode, FnBodyNode
//       FormalDeclNode    TypeNode, IdNode
//       StructDeclNode    IdNode, DeclListNode
//
//     FormalsListNode     linked list of FormalDeclNode
//     FnBodyNode          DeclListNode, StmtListNode
//     StmtListNode        linked list of StmtNode
//     ExpListNode         linked list of ExpNode
//
//     TypeNode:
//       IntNode           -- none --
//       BoolNode          -- none --
//       VoidNode          -- none --
//       StructNode        IdNode
//
//     StmtNode:
//       AssignStmtNode      AssignNode
//       PostIncStmtNode     ExpNode
//       PostDecStmtNode     ExpNode
//       ReadStmtNode        ExpNode
//       WriteStmtNode       ExpNode
//       IfStmtNode          ExpNode, DeclListNode, StmtListNode
//       IfElseStmtNode      ExpNode, DeclListNode, StmtListNode,
//                                    DeclListNode, StmtListNode
//       WhileStmtNode       ExpNode, DeclListNode, StmtListNode
//       RepeatStmtNode      ExpNode, DeclListNode, StmtListNode
//       CallStmtNode        CallExpNode
//       ReturnStmtNode      ExpNode
//
//     ExpNode:
//       IntLitNode          -- none --
//       StrLitNode          -- none --
//       TrueNode            -- none --
//       FalseNode           -- none --
//       IdNode              -- none --
//       DotAccessNode       ExpNode, IdNode
//       AssignNode          ExpNode, ExpNode
//       CallExpNode         IdNode, ExpListNode
//       UnaryExpNode        ExpNode
//         UnaryMinusNode
//         NotNode
//       BinaryExpNode       ExpNode ExpNode
//         PlusNode
//         MinusNode
//         TimesNode
//         DivideNode
//         AndNode
//         OrNode
//         EqualsNode
//         NotEqualsNode
//         LessNode
//         GreaterNode
//         LessEqNode
//         GreaterEqNode
//
// Here are the different kinds of AST nodes again, organized according to
// whether they are leaves, internal nodes with linked lists of children, or
// internal nodes with a fixed number of children:
//
// (1) Leaf nodes:
//        IntNode,   BoolNode,  VoidNode,  IntLitNode,  StrLitNode,
//        TrueNode,  FalseNode, IdNode
//
// (2) Internal nodes with (possibly empty) linked lists of children:
//        DeclListNode, FormalsListNode, StmtListNode, ExpListNode
//
// (3) Internal nodes with fixed numbers of children:
//        ProgramNode,     VarDeclNode,     FnDeclNode,     FormalDeclNode,
//        StructDeclNode,  FnBodyNode,      StructNode,     AssignStmtNode,
//        PostIncStmtNode, PostDecStmtNode, ReadStmtNode,   WriteStmtNode
//        IfStmtNode,      IfElseStmtNode,  WhileStmtNode,  RepeatStmtNode,
//        CallStmtNode
//        ReturnStmtNode,  DotAccessNode,   AssignExpNode,  CallExpNode,
//        UnaryExpNode,    BinaryExpNode,   UnaryMinusNode, NotNode,
//        PlusNode,        MinusNode,       TimesNode,      DivideNode,
//        AndNode,         OrNode,          EqualsNode,     NotEqualsNode,
//        LessNode,        GreaterNode,     LessEqNode,     GreaterEqNode
//
// **********************************************************************

// **********************************************************************
// ASTnode class (base class for all other kinds of nodes)
// **********************************************************************

abstract class ASTnode {
	// every subclass must provide an unparse operation
	abstract public void unparse(PrintWriter p, int indent);

	// this method can be used by the unparse methods to do indenting
	protected void addIndentation(PrintWriter p, int indent) {
		for (int k = 0; k < indent; k++)
			p.print(" ");
	}
}

// **********************************************************************
// ProgramNode, DeclListNode, FormalsListNode, FnBodyNode,
// StmtListNode, ExpListNode
// **********************************************************************

class ProgramNode extends ASTnode {
	public ProgramNode(DeclListNode L) {
		myDeclList = L;
	}

	public void unparse(PrintWriter p, int indent) {
		myDeclList.unparse(p, indent);
	}

	public void analyzeNames() {
		SymTable symTab = new SymTable();
		myDeclList.analyzeNames(symTab);
	}

	private DeclListNode myDeclList;
}

class DeclListNode extends ASTnode {
	public DeclListNode(List<DeclNode> S) {
		myDecls = S;
	}

	public void unparse(PrintWriter p, int indent) {
		Iterator<DeclNode> it = myDecls.iterator();
		try {
			while (it.hasNext()) {
				((DeclNode) it.next()).unparse(p, indent);
			}
		} catch (NoSuchElementException ex) {
			System.err.println(
					"unexpected NoSuchElementException in DeclListNode.print");
			System.exit(-1);
		}
	}

	public SymTable analyzeNamesBody(SymTable symTab) {
		HashMap<String, Integer> occured = new HashMap<>();
		for (DeclNode declNode : myDecls) {
			IdNode idNode = declNode.getIdNode();
			String name = idNode.toString();
			occured.put(name, occured.getOrDefault(name, 0) + 1);
			try {
				if (occured.get(name) == 1 && symTab.lookupLocal(name) == null) {
					declNode.analyzeNames(symTab);
				}
			} catch (EmptySymTableException e) {
				System.out.println(e);
			}
			if (occured.get(name) > 1) {
				ErrMsg.fatal(idNode.getLineNum(), idNode.getCharNum(),
						"Multiply declared identifier");
			}
		}
		return symTab;
	}

	public SymTable analyzeNames(SymTable symTab) {
		for (DeclNode declNode : myDecls) {
			declNode.analyzeNames(symTab);
		}
		return symTab;
	}

	public SymTable analyzeNames(SymTable symTab, SymTable structSymTab) {
		for (DeclNode declNode : myDecls) {
			VarDeclNode node = (VarDeclNode) declNode;
			if (node.getSize() == VarDeclNode.NOT_STRUCT) {
				node.analyzeNames(structSymTab);
			} else {
				node.analyzeStructNames(symTab, structSymTab);
			}
		}
		return symTab;
	}

	private List<DeclNode> myDecls;
}

class FormalsListNode extends ASTnode {
	public FormalsListNode(List<FormalDeclNode> S) {
		myFormals = S;
	}

	public void unparse(PrintWriter p, int indent) {
		Iterator<FormalDeclNode> it = myFormals.iterator();
		if (it.hasNext()) { // if there is at least one element
			it.next().unparse(p, indent);
			while (it.hasNext()) { // print the rest of the list
				p.print(", ");
				it.next().unparse(p, indent);
			}
		}
	}

	public LinkedList<String> getTypList() {
		LinkedList<String> typList = new LinkedList<>();
		for (FormalDeclNode formalDeclNode : myFormals) {
			typList.add(formalDeclNode.getType().toString());
		}
		return typList;
	}

	public SymTable analyzeNames(SymTable symTab) {
		for (FormalDeclNode formalDeclNode : myFormals) {
			formalDeclNode.analyzeNames(symTab);
		}
		return symTab;
	}

	private List<FormalDeclNode> myFormals;
}

class FnBodyNode extends ASTnode {
	public FnBodyNode(DeclListNode declList, StmtListNode stmtList) {
		myDeclList = declList;
		myStmtList = stmtList;
	}

	public void unparse(PrintWriter p, int indent) {
		myDeclList.unparse(p, indent);
		myStmtList.unparse(p, indent);
	}

	public void analyzeNames(SymTable symTab) {
		myDeclList.analyzeNamesBody(symTab);
		myStmtList.analyzeNames(symTab);
	}

	private DeclListNode myDeclList;
	private StmtListNode myStmtList;
}

class StmtListNode extends ASTnode {
	public StmtListNode(List<StmtNode> S) {
		myStmts = S;
	}

	public void unparse(PrintWriter p, int indent) {
		Iterator<StmtNode> it = myStmts.iterator();
		while (it.hasNext()) {
			it.next().unparse(p, indent);
		}
	}

	public void analyzeNames(SymTable symTab) {
		for (StmtNode stmtNode : myStmts) {
			stmtNode.analyzeNames(symTab);
		}
	}

	private List<StmtNode> myStmts;
}

class ExpListNode extends ASTnode {
	public ExpListNode(List<ExpNode> S) {
		myExps = S;
	}

	public void unparse(PrintWriter p, int indent) {
		Iterator<ExpNode> it = myExps.iterator();
		if (it.hasNext()) { // if there is at least one element
			it.next().unparse(p, indent);
			while (it.hasNext()) { // print the rest of the list
				p.print(", ");
				it.next().unparse(p, indent);
			}
		}
	}

	public void analyzeNames(SymTable symTab) {
		for (ExpNode expNode : myExps) {
			expNode.analyzeNames(symTab);
		}
	}

	private List<ExpNode> myExps;
}

// **********************************************************************
// DeclNode and its subclasses
// **********************************************************************

abstract class DeclNode extends ASTnode {
	public abstract SymTable analyzeNames(SymTable symTab);

	public abstract IdNode getIdNode();
}

class VarDeclNode extends DeclNode {
	public VarDeclNode(TypeNode type, IdNode id, int size) {
		myType = type;
		myId = id;
		mySize = size;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		myType.unparse(p, 0);
		p.print(" ");
		myId.unparse(p, 0);
		p.println(";");
	}

	public void analyzeVarNames(SymTable symTab) {
		TSym mySym = new TSym(this.myType.toString());
		try {
			symTab.addDecl(myId.toString(), mySym);
		} catch (DuplicateSymException e) {
			ErrMsg.fatal(myId.getLineNum(), myId.getCharNum(),
					"Multiply declared identifier");
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in VarDeclNode.analyzeNames");
			System.out.println(e);
		}
		return;
	}

	public void analyzeStructNames(SymTable symTab, SymTable structSymTab) {
		this.analyzeVarNames(symTab);
		this.analyzeVarNames(structSymTab);
		try {
			if (myType instanceof StructNode) {
				TSym structSym = structSymTab.lookupGlobal(
						((StructNode) this.myType).getId().toString());
				TSym varSym = symTab.lookupGlobal(myId.toString());
				if (structSym != null && varSym != null) {
					myId.setStruct(structSym.getStruct(), varSym);
				}
			}
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in VarDeclNode.analyzeStructNames");
			System.out.println(e);
		}
	}

	public boolean analyzeStructVarNames(SymTable symTab) {
		IdNode id = ((StructNode) this.myType).getId();
		try {
			TSym sym = symTab.lookupGlobal(id.toString());
			if (sym == null || sym.getStruct() == null) {
				ErrMsg.fatal(id.getLineNum(), id.getCharNum(),
						"Invalid name of struct type");
				return false;
			}
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in VarDeclNode.analyzeStructVarNames");
		}
		return true;
	}

	public SymTable analyzeNames(SymTable symTab) {
		if (myType instanceof VoidNode) {
			ErrMsg.fatal(myId.getLineNum(), myId.getCharNum(),
					"Non-function declared void");
			return symTab;
		}
		try {
			if (myType instanceof StructNode) {
				boolean valid = this.analyzeStructVarNames(symTab);
				TSym structSym = symTab.lookupGlobal(((StructNode) myType).getId().toString());
				if (valid && structSym == null) {
					return symTab;
				}
				this.analyzeVarNames(symTab);
				TSym varSym = symTab.lookupGlobal(myId.toString());
				myId.setStruct(structSym.getStruct(), varSym);
				return symTab;
			}
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in VarDeclNode.analyzeNames");
			System.out.println(e);
		}
		this.analyzeVarNames(symTab);
		return symTab;
	}

	public int getSize() {
		return mySize;
	}

	public IdNode getIdNode() {
		return this.myId;
	}

	private TypeNode myType;
	private IdNode myId;
	private int mySize; // use value NOT_STRUCT if this is not a struct type

	public static int NOT_STRUCT = -1;
}

class FnDeclNode extends DeclNode {
	public FnDeclNode(TypeNode type, IdNode id, FormalsListNode formalList,
			FnBodyNode body) {
		myType = type;
		myId = id;
		myFormalsList = formalList;
		myBody = body;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		myType.unparse(p, 0);
		p.print(" ");
		myId.unparse(p, 0);
		p.print("(");
		myFormalsList.unparse(p, 0);
		p.println(") {");
		myBody.unparse(p, indent + 4);
		p.println("}\n");
	}

	public SymTable analyzeNames(SymTable symTab) {
		LinkedList<String> params = myFormalsList.getTypList();
		try {
			symTab.addDecl(this.myId.toString(),
					new fnSym(myType.toString(), params));
		} catch (DuplicateSymException e) {
			ErrMsg.fatal(myId.getLineNum(), myId.getCharNum(),
					"Multiply declared identifier");
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in FnDeclNode.analyzeNames");
			System.out.println(e);
		}
		symTab.addScope();
		myFormalsList.analyzeNames(symTab);
		myBody.analyzeNames(symTab);
		try {
			symTab.removeScope();
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in FnDeclNode.analyzeNames");
			System.out.println(e);
		}
		return symTab;
	}

	public IdNode getIdNode() {
		return this.myId;
	}

	private TypeNode myType;
	private IdNode myId;
	private FormalsListNode myFormalsList;
	private FnBodyNode myBody;
}

class FormalDeclNode extends DeclNode {
	public FormalDeclNode(TypeNode type, IdNode id) {
		myType = type;
		myId = id;
	}

	public void unparse(PrintWriter p, int indent) {
		myType.unparse(p, 0);
		p.print(" ");
		myId.unparse(p, 0);
	}

	public SymTable analyzeNames(SymTable symTab) {
		try {
			symTab.addDecl(myId.toString(), new TSym(this.myType.toString()));
		} catch (DuplicateSymException e) {
			ErrMsg.fatal(myId.getLineNum(), myId.getCharNum(),
					"Multiply declared identifier");
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in FormalDeclNode.analyzeNames");
			System.out.println(e);
		}
		return symTab;
	}

	public String getType() {
		return myType.toString();
	}

	public IdNode getIdNode() {
		return this.myId;
	}

	private TypeNode myType;
	private IdNode myId;
}

class StructDeclNode extends DeclNode {
	public StructDeclNode(IdNode id, DeclListNode declList) {
		myId = id;
		myDeclList = declList;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("struct ");
		myId.unparse(p, 0);
		p.println("{");
		myDeclList.unparse(p, indent + 4);
		addIndentation(p, indent);
		p.println("};\n");
	}

	public SymTable analyzeNames(SymTable symTab) {
		TSym structSym = new TSym("struct-decl");
		try {
			symTab.addDecl(myId.toString(), structSym);
		} catch (DuplicateSymException e) {
			ErrMsg.fatal(myId.getLineNum(), myId.getCharNum(),
					"Multiply declared identifier");
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in StructDeclNode.analyzeNames");
			System.out.println(e);
		}
		newSymTab = new SymTable();
		myId.setStruct(this, structSym);
		myDeclList.analyzeNames(symTab, newSymTab);
		return symTab;
	}

	public SymTable getSymTable() {
		return newSymTab;
	}

	public IdNode getIdNode() {
		return myId;
	}

	private IdNode myId;
	private DeclListNode myDeclList;
	private SymTable newSymTab;
}

// **********************************************************************
// TypeNode and its Subclasses
// **********************************************************************

abstract class TypeNode extends ASTnode {
}

class IntNode extends TypeNode {
	public IntNode() {
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("int");
	}

	public String getType() {
		return "int";
	}
}

class BoolNode extends TypeNode {
	public BoolNode() {
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("bool");
	}

	public String getType() {
		return "bool";
	}
}

class VoidNode extends TypeNode {
	public VoidNode() {
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("void");
	}

	public String getType() {
		return "void";
	}
}

class StructNode extends TypeNode {
	public StructNode(IdNode id) {
		myId = id;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("struct ");
		myId.unparse(p, 0);
	}

	public String getType() {
		return myId.toString();
	}

	public IdNode getId() {
		return myId;
	}

	private IdNode myId;
}

// **********************************************************************
// StmtNode and its subclasses
// **********************************************************************

abstract class StmtNode extends ASTnode {
	public abstract void analyzeNames(SymTable symTab);
}

class AssignStmtNode extends StmtNode {
	public AssignStmtNode(AssignNode assign) {
		myAssign = assign;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		myAssign.unparse(p, -1); // no parentheses
		p.println(";");
	}

	public void analyzeNames(SymTable symTab) {
		myAssign.analyzeNames(symTab);
	}

	private AssignNode myAssign;
}

class PostIncStmtNode extends StmtNode {
	public PostIncStmtNode(ExpNode exp) {
		myExp = exp;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		myExp.unparse(p, 0);
		p.println("++;");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
	}

	private ExpNode myExp;
}

class PostDecStmtNode extends StmtNode {
	public PostDecStmtNode(ExpNode exp) {
		myExp = exp;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		myExp.unparse(p, 0);
		p.println("--;");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
	}

	private ExpNode myExp;
}

class ReadStmtNode extends StmtNode {
	public ReadStmtNode(ExpNode e) {
		myExp = e;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("cin >> ");
		myExp.unparse(p, 0);
		p.println(";");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
	}

	// 1 child (actually can only be an IdNode or an ArrayExpNode)
	private ExpNode myExp;
}

class WriteStmtNode extends StmtNode {
	public WriteStmtNode(ExpNode exp) {
		myExp = exp;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("cout << ");
		myExp.unparse(p, 0);
		p.println(";");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
	}

	private ExpNode myExp;
}

class IfStmtNode extends StmtNode {
	public IfStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
		myDeclList = dlist;
		myExp = exp;
		myStmtList = slist;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("if (");
		myExp.unparse(p, 0);
		p.println(") {");
		myDeclList.unparse(p, indent + 4);
		myStmtList.unparse(p, indent + 4);
		addIndentation(p, indent);
		p.println("}");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
		symTab.addScope();
		myDeclList.analyzeNames(symTab);
		myStmtList.analyzeNames(symTab);
		try {
			symTab.removeScope();
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in IfStmtNode.analyzeNames");
			System.out.println(e);
		}
	}

	private ExpNode myExp;
	private DeclListNode myDeclList;
	private StmtListNode myStmtList;
}

class IfElseStmtNode extends StmtNode {
	public IfElseStmtNode(ExpNode exp, DeclListNode dlist1, StmtListNode slist1,
			DeclListNode dlist2, StmtListNode slist2) {
		myExp = exp;
		myThenDeclList = dlist1;
		myThenStmtList = slist1;
		myElseDeclList = dlist2;
		myElseStmtList = slist2;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("if (");
		myExp.unparse(p, 0);
		p.println(") {");
		myThenDeclList.unparse(p, indent + 4);
		myThenStmtList.unparse(p, indent + 4);
		addIndentation(p, indent);
		p.println("}");
		addIndentation(p, indent);
		p.println("else {");
		myElseDeclList.unparse(p, indent + 4);
		myElseStmtList.unparse(p, indent + 4);
		addIndentation(p, indent);
		p.println("}");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
		symTab.addScope();
		myThenDeclList.analyzeNames(symTab);
		myThenStmtList.analyzeNames(symTab);
		try {
			symTab.removeScope();
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in IfElseStmtNode.analyzeNames");
			System.out.println(e);
		}
		symTab.addScope();
		myElseStmtList.analyzeNames(symTab);
		myElseDeclList.analyzeNames(symTab);
		try {
			symTab.removeScope();
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in IfElseStmtNode.analyzeNames");
			System.out.println(e);
		}
	}

	private ExpNode myExp;
	private DeclListNode myThenDeclList;
	private StmtListNode myThenStmtList;
	private StmtListNode myElseStmtList;
	private DeclListNode myElseDeclList;
}

class WhileStmtNode extends StmtNode {
	public WhileStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
		myExp = exp;
		myDeclList = dlist;
		myStmtList = slist;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("while (");
		myExp.unparse(p, 0);
		p.println(") {");
		myDeclList.unparse(p, indent + 4);
		myStmtList.unparse(p, indent + 4);
		addIndentation(p, indent);
		p.println("}");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
		symTab.addScope();
		myDeclList.analyzeNames(symTab);
		myStmtList.analyzeNames(symTab);
		try {
			symTab.removeScope();
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in WhileStmtNode.analyzeNames");
			System.out.println(e);
		}
	}

	private ExpNode myExp;
	private DeclListNode myDeclList;
	private StmtListNode myStmtList;
}

class RepeatStmtNode extends StmtNode {
	public RepeatStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
		myExp = exp;
		myDeclList = dlist;
		myStmtList = slist;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("repeat (");
		myExp.unparse(p, 0);
		p.println(") {");
		myDeclList.unparse(p, indent + 4);
		myStmtList.unparse(p, indent + 4);
		addIndentation(p, indent);
		p.println("}");
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
		symTab.addScope();
		myDeclList.analyzeNames(symTab);
		myStmtList.analyzeNames(symTab);
		try {
			symTab.removeScope();
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in RepeatStmtNode.analyzeNames");
			System.out.println(e);
		}
	}

	private ExpNode myExp;
	private DeclListNode myDeclList;
	private StmtListNode myStmtList;
}

class CallStmtNode extends StmtNode {
	public CallStmtNode(CallExpNode call) {
		myCall = call;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		myCall.unparse(p, indent);
		p.println(";");
	}

	public void analyzeNames(SymTable symTab) {
		myCall.analyzeNames(symTab);
	}

	private CallExpNode myCall;
}

class ReturnStmtNode extends StmtNode {
	public ReturnStmtNode(ExpNode exp) {
		myExp = exp;
	}

	public void unparse(PrintWriter p, int indent) {
		addIndentation(p, indent);
		p.print("return");
		if (myExp != null) {
			p.print(" ");
			myExp.unparse(p, 0);
		}
		p.println(";");
	}

	public void analyzeNames(SymTable symTab) {
		if (myExp != null) {
			myExp.analyzeNames(symTab);
		}
	}

	private ExpNode myExp; // possibly null
}

// **********************************************************************
// ExpNode and its subclasses
// **********************************************************************

abstract class ExpNode extends ASTnode {

	public abstract void analyzeNames(SymTable symTab);
}

class IntLitNode extends ExpNode {
	public IntLitNode(int lineNum, int charNum, int intVal) {
		myLineNum = lineNum;
		myCharNum = charNum;
		myIntVal = intVal;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print(myIntVal);
	}

	public void analyzeNames(SymTable symTab) {
		// do nothing
	}

	private int myLineNum;
	private int myCharNum;
	private int myIntVal;
}

class StringLitNode extends ExpNode {
	public StringLitNode(int lineNum, int charNum, String strVal) {
		myLineNum = lineNum;
		myCharNum = charNum;
		myStrVal = strVal;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print(myStrVal);
	}

	public void analyzeNames(SymTable symTab) {
		// do nothing
	}

	private int myLineNum;
	private int myCharNum;
	private String myStrVal;
}

class TrueNode extends ExpNode {
	public TrueNode(int lineNum, int charNum) {
		myLineNum = lineNum;
		myCharNum = charNum;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("true");
	}

	public void analyzeNames(SymTable symTab) {
		// do nothing
	}

	private int myLineNum;
	private int myCharNum;
}

class FalseNode extends ExpNode {
	public FalseNode(int lineNum, int charNum) {
		myLineNum = lineNum;
		myCharNum = charNum;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("false");
	}

	public void analyzeNames(SymTable symTab) {
		// do nothing
	}

	private int myLineNum;
	private int myCharNum;
}

class IdNode extends ExpNode {
	public IdNode(int lineNum, int charNum, String strVal) {
		myLineNum = lineNum;
		myCharNum = charNum;
		myStrVal = strVal;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print(myStrVal);
		if (mySym != null) {
			p.print("(" + mySym.toString() + ")");
		}
	}

	public int getLineNum() {
		return myLineNum;
	}

	public int getCharNum() {
		return myCharNum;
	}

	public String toString() {
		return myStrVal;
	}

	public void analyzeNames(SymTable symTab) {
		try {
			this.mySym = symTab.lookupGlobal(myStrVal);
			if (this.mySym == null) {
				ErrMsg.fatal(myLineNum, myCharNum, "Undeclared identifier");
			} else {
				this.myStruct = mySym.getStruct();
			}
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in IdNode.analyzeNames");
			System.out.println(e);
		}
		return;
	}

	public TSym getSym() {
		return mySym;
	}

	public void setSym(TSym mySym) {
		this.mySym = mySym;
	}

	public StructDeclNode getStruct() {
		return myStruct;
	}

	public void setStruct(StructDeclNode myStruct, TSym sym) {
		this.myStruct = myStruct;
		sym.setStruct(myStruct);
	}

	public StructDeclNode getStruct(TSym sym) {
		return myStruct;
	}

	private int myLineNum;
	private int myCharNum;
	private String myStrVal;
	private TSym mySym;
	private StructDeclNode myStruct;
}

class DotAccessExpNode extends ExpNode {
	public DotAccessExpNode(ExpNode loc, IdNode id) {
		myLoc = loc;
		myId = id;
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myLoc.unparse(p, 0);
		p.print(").");
		myId.unparse(p, 0);
	}

	public void analyzeNames(SymTable symTab) {
		myLoc.analyzeNames(symTab);
		StructDeclNode lhs = getLHSStruct(symTab);
		if (lhs == null) {
			return;
		}
		SymTable leftTable = lhs.getSymTable();
		try {
			TSym lookupSym = leftTable.lookupGlobal(myId.toString());
			if (lookupSym == null) {
				ErrMsg.fatal(((IdNode) myLoc).getLineNum(), ((IdNode) myLoc).getCharNum(),
						"Invalid struct field name");
				return;
			}
		} catch (EmptySymTableException e) {
			System.err.println("Unexpected EmptySymTableException "
					+ " in IdNode.analyzeNames");
			System.out.println(e);
		}
	}

	// :TODO:
	private StructDeclNode getLHSStruct(SymTable symTab) {
		if (myLoc instanceof IdNode) {
			try {
				TSym lookupSym = symTab.lookupGlobal(((IdNode) myLoc).toString());
				if (lookupSym == null) {
					return null;
				}
				if (lookupSym.getStruct() == null) {
					ErrMsg.fatal(((IdNode) myLoc).getLineNum(),
							((IdNode) myLoc).getCharNum(),
							"Dot-access of non-struct type");
					return null;
				}
			} catch (EmptySymTableException e) {
				System.err.println("Unexpected EmptySymTableException "
						+ " in IdNode.analyzeNames");
				System.out.println(e);
			}
		} else {
			StructDeclNode lhs = ((DotAccessExpNode) myLoc).getLHSStruct(symTab);
			if (lhs == null) {
				return null;
			}
			SymTable lhsSymTab = lhs.getSymTable();
			try {
				TSym lookupSym = lhsSymTab.lookupGlobal(((IdNode) myLoc).toString());
				if (lookupSym == null) {
					return null;
				} else {
					return lookupSym.getStruct();
				}
			} catch (EmptySymTableException e) {
				System.err.println("Unexpected EmptySymTableException "
						+ " in IdNode.analyzeNames");
				System.out.println(e);
			}
		}
		return null; // :FIX:Please dear god fix me
	}

	private ExpNode myLoc;
	private IdNode myId;
}

class AssignNode extends ExpNode {
	public AssignNode(ExpNode lhs, ExpNode exp) {
		myLhs = lhs;
		myExp = exp;
	}

	public void unparse(PrintWriter p, int indent) {
		if (indent != -1)
			p.print("(");
		myLhs.unparse(p, 0);
		p.print(" = ");
		myExp.unparse(p, 0);
		if (indent != -1)
			p.print(")");
	}

	public void analyzeNames(SymTable symTab) {
		myLhs.analyzeNames(symTab);
		myExp.analyzeNames(symTab);
	}

	private ExpNode myLhs;
	private ExpNode myExp;
}

class CallExpNode extends ExpNode {
	public CallExpNode(IdNode name, ExpListNode elist) {
		myId = name;
		myExpList = elist;
	}

	public CallExpNode(IdNode name) {
		myId = name;
		myExpList = new ExpListNode(new LinkedList<ExpNode>());
	}

	public void unparse(PrintWriter p, int indent) {
		myId.unparse(p, 0);
		p.print("(");
		if (myExpList != null) {
			myExpList.unparse(p, 0);
		}
		p.print(")");
	}

	public void analyzeNames(SymTable symTab) {
		myId.analyzeNames(symTab);
		myExpList.analyzeNames(symTab);
	}

	private IdNode myId;
	private ExpListNode myExpList; // possibly null
}

abstract class UnaryExpNode extends ExpNode {
	public UnaryExpNode(ExpNode exp) {
		myExp = exp;
	}

	public void analyzeNames(SymTable symTab) {
		myExp.analyzeNames(symTab);
	}

	protected ExpNode myExp;
}

abstract class BinaryExpNode extends ExpNode {
	public BinaryExpNode(ExpNode exp1, ExpNode exp2) {
		myExp1 = exp1;
		myExp2 = exp2;
	}

	public void analyzeNames(SymTable symTab) {
		myExp1.analyzeNames(symTab);
		myExp2.analyzeNames(symTab);
	}

	protected ExpNode myExp1;
	protected ExpNode myExp2;
}

// **********************************************************************
// Subclasses of UnaryExpNode
// **********************************************************************

class UnaryMinusNode extends UnaryExpNode {
	public UnaryMinusNode(ExpNode exp) {
		super(exp);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(-");
		myExp.unparse(p, 0);
		p.print(")");
	}
}

class NotNode extends UnaryExpNode {
	public NotNode(ExpNode exp) {
		super(exp);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(!");
		myExp.unparse(p, 0);
		p.print(")");
	}
}

// **********************************************************************
// Subclasses of BinaryExpNode
// **********************************************************************

class PlusNode extends BinaryExpNode {
	public PlusNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" + ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class MinusNode extends BinaryExpNode {
	public MinusNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" - ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class TimesNode extends BinaryExpNode {
	public TimesNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" * ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class DivideNode extends BinaryExpNode {
	public DivideNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" / ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class AndNode extends BinaryExpNode {
	public AndNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" && ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class OrNode extends BinaryExpNode {
	public OrNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" || ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class EqualsNode extends BinaryExpNode {
	public EqualsNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" == ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class NotEqualsNode extends BinaryExpNode {
	public NotEqualsNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" != ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class LessNode extends BinaryExpNode {
	public LessNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" < ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class GreaterNode extends BinaryExpNode {
	public GreaterNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" > ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class LessEqNode extends BinaryExpNode {
	public LessEqNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" <= ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}

class GreaterEqNode extends BinaryExpNode {
	public GreaterEqNode(ExpNode exp1, ExpNode exp2) {
		super(exp1, exp2);
	}

	public void unparse(PrintWriter p, int indent) {
		p.print("(");
		myExp1.unparse(p, 0);
		p.print(" >= ");
		myExp2.unparse(p, 0);
		p.print(")");
	}
}
