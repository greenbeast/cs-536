public class DuplicateSymException extends Exception {
  /*
    When a duplicate symbol is encountered, this exception is thrown.
   */
}
