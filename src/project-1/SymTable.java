import java.util.*;

public class SymTable {
  /*
    Symbol table used to store the symbols in the source code
  */

  // our list of HashMap to store different tokens in different scopes
  private List<HashMap<String, Sym>> symTable;

  public SymTable() {
    /*
    This is the constructor which initializes the symTable's List field to
    contain a single, empty HashMap.
    */
    this.symTable = new ArrayList<>();
    this.symTable.add(0, new HashMap<>());
  }

  void addDecl(String name, Sym sym)
      throws DuplicateSymException, EmptySymTableException {
    /*
      Here we are checking if our table is empty. If it is, we throw an
      exception. If it is not, we add the declaration to the first scope in our
      list
     */
    if (symTable.isEmpty()) {
      throw new EmptySymTableException();
    }

    if (name == null || sym == null) {
      throw new NullPointerException();
    }

    if (this.symTable.get(0).containsKey(name)) {
      throw new DuplicateSymException();
    }

    // if our table is not empty, doesn't contain null for name or sym, and the
    // name is not already in the table, we add it to the table
    this.symTable.get(0).put(name, sym);
  }

  void addScope() {
    /*
    Add the HashMap to our list
    */
    this.symTable.add(0, new HashMap<>());
  }

  Sym lookupLocal(String name) throws EmptySymTableException {
    /*
    Looks up the symbol in our first HashMap
    */
    if (this.symTable.isEmpty()) {
      throw new EmptySymTableException();
    }

    return this.symTable.get(0).getOrDefault(name, null);
  }

  Sym lookupGlobal(String name) throws EmptySymTableException {
    /*
    Looks up the symbol in every HashMap
    */
    if (this.symTable.isEmpty()) {
      throw new EmptySymTableException();
    }

    for (HashMap<String, Sym> map : symTable) {
      if (map.containsKey(name)) {
        return map.get(name);
      }
    }
    return null;
  }

  void removeScope() throws EmptySymTableException {
    /*
    Removes the HashMap from the front of our list
    */
    if (this.symTable.isEmpty()) {
      throw new EmptySymTableException();
    }
    this.symTable.remove(0);
  }

  void print() {
    /*
      This is taking our symTable and printing out each HashMap in our list
     */
    StringBuilder output = new StringBuilder();
    output.append("\nSymTable:\n");

    for (HashMap<String, Sym> map : symTable) {
      output.append(map.toString()).append("\n");
    }

    output.append("\n");
    System.out.println(output);
  }
}
