public class EmptySymTableException extends Exception {
  /*
    When an empty symbol table is encountered, this exception is thrown, as at least one scope is required
   */
}
