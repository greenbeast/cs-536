public class Sym {
  /*
  This class represents symbols in our code
  */

  // the type the symbol belongs to such as int-literal, identifier, etc.
  private final String type;

  // construction method
  public Sym(String type) {
      this.type = type;
  }

  // gets the type of the symbol
  public String getType() {
      return this.type;
  }

  // overrides the toSTring() method
  @Override
  public String toString() {
    return this.type;
  }
}
